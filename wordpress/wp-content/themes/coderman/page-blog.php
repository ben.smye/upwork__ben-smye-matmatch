<?php

$context = Timber::context();
$post = new Timber\Post();

$context['hero'] = [
  'title' => get_field('hero')['title'],
  'content' => get_field('hero')['content'],
  'cta' => get_field('hero')['link'],
  'bg' => get_field('hero')['background'],
];

$context['latest'] = Timber::get_posts([
  'post_type' => 'post',
  'post_status' => 'publish',
  'posts_per_page' => '1'
]);

$context['bar'] = get_field('bar');

// Categories
$categories = get_categories([
  'orderby' => 'name',
  'order'   => 'ASC'
]);

$listing = [];

foreach( $categories as $key=>$category ) {
  // Get 6 posts for the second category, only.
  $posts_no = $key == 1 ? '6' : '3';

  if ($category->name != 'Uncategorized') {
    array_push($listing,
      [
        'name' => $category->name,
        'url' => get_category_link(get_cat_ID($category->name)),
        'posts' => new Timber\postQuery([
          'cat' => $category->term_id,
          'post_status' => 'publish',
          'posts_per_page' => $posts_no,
          // 'paged' => 1
        ]),
      ]
    );
  }
}

// Add other data to 'Guest Author' section.
$listing[0]['meta'] = get_field('guest_author');

// Add other data to 'Materials & Applications'
$listing[1]['meta'] = get_field('materials_and_applications');

$context['categories'] = $listing;


Timber::render( 'templates/blog.twig', $context );
