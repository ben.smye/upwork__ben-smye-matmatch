<?php

$context = Timber::context();
$post = new Timber\Post();

$context['hero'] = [
  'title' => get_field('hero')['title'],
  'content' => get_field('hero')['content'],
  'cta' => get_field('hero')['link'],
  'bg' => get_field('hero')['background'],
];

$context['bar'] = get_field('bar');

$context['featured'] = [
  'title' => get_field('featured_title')
];

$menu_learn = new Timber\Menu( 'learn' );

// Encode Learn menu items as JSON, to be used by React.
$context['wp_data'] = base64_encode(
  json_encode(
    array_map(function($main){
      return [
        'title' => $main->title,
        'url' => $main->url,
        'id' => md5($main->title),
        'level_1' => array_map(function($item){
          return [
            'title' => $item->title,
            'url' => $item->url,
            'id' => md5($item->title),
          ];
        }, $main->children)
      ];
    }, $menu_learn->items)
  )
);

$context['wp_featured'] = base64_encode(
  json_encode(
    get_field('featured_api')
  )
);

// print_r('<pre>');
// print_r(json_encode($context['wp_featured']));
// print_r('</pre>');

Timber::render( 'templates/learn.twig', $context );
