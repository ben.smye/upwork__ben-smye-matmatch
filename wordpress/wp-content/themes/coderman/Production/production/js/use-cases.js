import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import CONST from './modules/const';
import { truncate } from './modules/utils.js';

import LearnArticle from './Components/Learn/learn-article';
import MenuAside from './Components/Learn/menu-aside-level-0';
import AppHero from './Components/Learn/app-hero';
import AppPinned from './Components/Learn/app-pinned';
import AppPosts from './Components/Learn/app-posts';
import Select from 'react-select';

const squidex = {
  featured: document.querySelector('#squidex-use-cases-featured'),
  listing: document.querySelector('#squidex-use-cases-listing'),
}

// Use Cases: Featured
if (squidex.featured) {
  const featured_url = squidex.featured.getAttribute('data-featured');

  class FeaturedArticles extends Component {
    constructor(props) {
      super(props);

      this.featured_url = JSON.parse(atob(this.props.featured_url));

      this.state = {
        loaded: false,
        featured: [
          {},{}
        ],
        listing: []
      }
    }

    componentDidMount() {
      axios.get(this.featured_url)
        .then(response => {
          // handle success
          const newState = { ...this.state };
          newState.featured = response.data.posts;

          newState.loaded = true;

          this.setState(newState);
        })
        .catch(function (error) {
          // handle error
        })
        .then(function () {
          // always executed
        });
    }

    render() {
      const content = this.state.featured.map((item, index) => {

        return (
          <div className="col-xs-6" key={index}>
            <LearnArticle
              img={item.image}
              title={item.title}
              url={item.url}
              content={truncate(item.description, 120)}
              loaded={this.state.loaded}
              tags={item.tags}
              time={item.read_time}
            ></LearnArticle>
          </div>
        );
      });

      return (
        <div className="row row--vertical">
          { content }
        </div>
      );
    }
  }

  ReactDOM.render(<FeaturedArticles featured_url={featured_url} />, squidex.featured);
}

// Use-Cases: Listing
if (squidex.listing) {
  const data = squidex.listing.getAttribute('data-wp');
  const squidexResourcesUrl = squidex.listing.getAttribute('data-squidex-resources');

  class FeaturedListing extends Component {
    constructor(props) {
      super(props);

      this.handleMenuChange = this.handleMenuChange.bind(this);
      this.handlePinnedImageLoad = this.handlePinnedImageLoad.bind(this);
      this.handlePostImageLoad = this.handlePostImageLoad.bind(this);
      this.handleImageLoad = this.handleImageLoad.bind(this);
      this.handleSubChange = this.handleSubChange.bind(this);
      this.handleMobileSubChange = this.handleMobileSubChange.bind(this);
      this.handleReadMore = this.handleReadMore.bind(this);
      this.resetLoaded = this.resetLoaded.bind(this);
      this.getDataSub = this.getDataSub.bind(this);

      this.indexDefault = 10;

      this.state = {
        // Decode Base64 data.
        menus: this.getMenus(),
        menusMobile: this.getMenusMobile(),
        active: {
          menu: this.getMenus()[0].id,
          sub: ''
        },
        content: {
          hero: {
            loaded: false,
            title: '',
            bg: {
              src: '',
              alt: 'Background image'
            }
          },
          pinned: Array(2).fill({
            img: {
              src: ''
            },
            url: '',
            title: '',
            description: ''
          }),
          posts: []
        },
        loaded: {
          pinned: [false, false],
          posts: [false, false, false, false, false, false]
        },
        staging: {
          index: this.indexDefault, // Number of posts per page load/load more
          increment: 10,
          posts: Array(6).fill({
            img: {
              src: ''
            },
            url: '',
            title: '',
            tags: [],
            description: ''
          })
        }
      };
    }

    getMenus() {
      return JSON.parse(atob(this.props.data));
    }

    getMenusMobile() {
      return JSON.parse(atob(this.props.data)).map(item => {
        return {
          label: item.title,
          value: item.url,
          id: item.id
        };
      });
    }

    resetLoaded() {
      const newState = {...this.state};

      newState.content.hero.loaded = false;
      newState.loaded.pinned = newState.loaded.pinned.map(item => false);
      newState.loaded.posts = newState.loaded.posts.map(item => false);

      this.setState(newState);
    }

    handleMenuChange(e) {
      e.preventDefault();

      const id = e.target.id;

      // Update active only on change
      if (id !== this.state.active.menu) {
        const newState = {...this.state};

        // Set new active menu
        newState.active.menu = id;

        // Reset the 'Read More' index
        newState.staging.index = this.indexDefault;

        // Get index of the currently active menu
        const indexActiveMenu = newState.menus.findIndex(menu => menu.id === newState.active.menu);

        // Set new active sub
        // newState.active.sub = newState.menus[indexActiveMenu].level_1[0].id;

        this.setState(newState);

        this.getDataSub(this.state.menus[indexActiveMenu].level_1[0].id, this.state.menus[indexActiveMenu].level_1[0].url)
      }
    }

    getDataSub(id, url) {
      const newState = {...this.state};

      // Set new active sub-menu (aside)
      newState.active.sub = id;

      // Reset all the loaded flags.
      this.resetLoaded();

      axios.get(url)
        .then(response => {
          // handle success
          const newState = { ...this.state };

          newState.content.posts = response.data.posts;
          newState.content.hero = response.data.hero;
          newState.content.hero.loaded = true;

          // Filter articles that are pinned.
          newState.content.pinned = response.data.posts.filter(post => {
            return post.pinned === true;
          });

          // Reset the 'Read More' index
          newState.staging.index = this.indexDefault;

          this.appendStaging(newState);
        })
        .catch(function (error) {
          // handle error
        })
        .then(function () {
          // always executed
        });
    }

    appendStaging (state) {
      let index = state.staging.index;
      const increment = state.staging.increment;

      state.staging.posts = state.content.posts.slice(0, index);

      index = index + increment;

      state.staging.index = index;

      this.setState(state);
    }

    handleReadMore () {
      this.appendStaging(this.state);
    }

    componentDidMount() {
      // Get API URL from query string
      const urlParams = new URLSearchParams(window.location.search);
      const urlRoot = urlParams.get('root');
      const urlMenu = urlParams.get('menu');

      // Remove URL parameters not needed for API request.
      urlParams.delete('root');
      urlParams.delete('menu');

      const urlQuery = urlParams.toString();

      // Load first sub-category of the first category.
      let id = this.state.menus[0].id;
      let url = this.state.menus[0].url;

      if (urlParams.toString().length) {
        url = `${squidexResourcesUrl}/${urlRoot}?${urlQuery}`;

        const activeMenu = this.state.menus.find(item => {
          return item.title === urlMenu;
        });

        const newState = {...this.state};

        newState.active.menu = activeMenu.id;

        this.setState(newState, () => this.getDataSub(activeMenu.id, url));

      } else if (window.sessionStorage.getItem('useCasesMenuActiveId') && window.sessionStorage.getItem('useCasesMenuActiveUrl')) {

        id = window.sessionStorage.getItem('useCasesMenuActiveId');
        url = window.sessionStorage.getItem('useCasesMenuActiveUrl');

        this.getDataSub(id, url);

      } else {
        this.getDataSub(id, url);
      }

      // TODO: remove
      // this.getMenus()[0].id;
    }

    handleSubChange(e) {
      e.preventDefault();

      const id = e.target.id;
      const url = e.target.href;

      // Update active only on change
      if (id !== this.state.active.sub) {
        // Save to session storage.
        window.sessionStorage.setItem('useCasesMenuActiveId', id);
        window.sessionStorage.setItem('useCasesMenuActiveUrl', url);

        this.getDataSub(id, url);
      }
    }

    handleMobileSubChange(e) {
      const id = e.id;
      const url = e.value;

      // Update active only on change
      if (id !== this.state.active.sub) {
        // Save to session storage.
        window.sessionStorage.saveItem('useCasesMenuActiveId', id);
        window.sessionStorage.saveItem('useCasesMenuActiveUrl', url);

        this.getDataSub(id, url);
      }
    }

    handleImageLoad() {
      const newState = {...this.state};

      newState.content.hero.loaded = true;

      this.setState(newState);
    }

    handlePinnedImageLoad(e) {
      const id = (e.target.id);

      const newState = {...this.state};

      newState.loaded.pinned[id] = true;

      this.setState(newState);
    }

    handlePostImageLoad(e) {
      const id = (e.target.id);

      const newState = {...this.state};

      newState.loaded.posts[id] = true;

      this.setState(newState);
    }

    render() {
      return (
        <div className="app">
          <div className="container">
            <div className="row">
              <div className="col-sm-6 col-md-2">
                <div className="hidden-md mb-4x">
                  <Select
                    options={this.state.menusMobile}
                    defaultValue={this.state.menusMobile[0]}
                    onChange={this.handleMobileSubChange}
                  />
                </div>
                <div className="visible-md">
                  <MenuAside
                    items={this.state.menus}
                    activeSub={this.state.active.sub}
                    onClick={this.handleSubChange}
                  />
                </div>
              </div>
              <div className="col-md-10">
                <AppHero
                  loaded={ this.state.content.hero.loaded }
                  onLoad={this.handleImageLoad}
                  src={ this.state.content.hero.bg.src }
                  title={ this.state.content.hero.title }
                />
                <AppPinned
                  articles={this.state.content.pinned}
                  loaded={ this.state.loaded.pinned }
                  onLoad={this.handlePinnedImageLoad}
                />
                <AppPosts
                  posts={this.state.staging.posts}
                  loaded={ this.state.loaded.posts }
                  onLoad={this.handlePostImageLoad}
                  postsToShow={this.state.content.posts.length - this.state.staging.posts.length}
                  onClick={this.handleReadMore}
                />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  ReactDOM.render(<FeaturedListing data={data} />, squidex.listing);
}
