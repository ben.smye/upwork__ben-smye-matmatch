import React from 'react';
import Tags from '../Tags/tags';
import Image from '../Image/Image';
import Time from '../Time/Time';

const AppArticle = props => {

  const loading = props.loaded[props.id] ? '' : 'is-loading';

  return (
    <div className="card-link">
      <div className={`card card--featured card--ajax ${loading}`}>
        <div className="row">
          <div className="app__article-img app__article-img--lg">
            <a href={props.url}>
              <Image
                onLoad={props.onLoad}
                image={props.image}
                width="220"
                height="132"
                classes="img-responsive card__img card__img--featured"
                id={props.id}
              />
            </a>
          </div>
          <div className="app__article-content app__article-content--lg">
            <div className="card__content">
              <a className="card-link card-link--static" href={props.url}>
                <h4 className="mb-05x card__title">{ props.title }</h4>
              </a>
              <Tags links={ props.tags }  />
              <p className="card__description">{ props.content }</p>
              <Time time={props.time} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AppArticle;
