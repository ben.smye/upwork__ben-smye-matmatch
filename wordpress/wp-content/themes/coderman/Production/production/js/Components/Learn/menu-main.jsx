import React from 'react';

const MenuMain = props => {

  const { items, active, onClick } = props;

  const list = items.map((item, index) => {
    return (
      <li key={index}
        className={item.id === active ? 'active' : null}>
          <a href={item.url} id={item.id} onClick={onClick}>{item.title}</a>
      </li>
    );
  });

  return (
    <div className="app__header">
      <nav>
        <ul className="app__nav list-reset">
          {list}
        </ul>
      </nav>
    </div>
  );
};

export default MenuMain;
