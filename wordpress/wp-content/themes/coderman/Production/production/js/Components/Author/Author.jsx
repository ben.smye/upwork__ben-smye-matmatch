import React from 'react';

const Author = props => {
  const initials = name => {
    if (name) {
      const words = name.split(' ');
      const initials = [
        words[0] ? words[0].substr(0,1) : '',
        words[1] ? words[1].substr(0,1) : '',
      ];

      return initials.join('');
    }
  };

  if (props.author) {
    return (
      <dl className="card__author">
        <dt><span>{initials(props.author)}</span></dt>
        <dd>{props.author}</dd>
      </dl>
    );
  }

  return null;
};

export default Author;
