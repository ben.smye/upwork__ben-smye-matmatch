import React from 'react';
import AppArticle from './app-article-block';

const ReadMore = props => {
  if (props.postsToShow > 0) {
    return <button type="button" className="app__btn" onClick={props.onClick}>Read More</button>;
  }

  return null;
};

const AppPosts = props => {
  const posts = props.posts.map((post, index) => {
    return (
      <li key={index}>
        <AppArticle
          url={post.url}
          image={post.image}
          title={post.title}
          tags={post.tags}
          time={post.read_time}
          content={post.description}
          onLoad={props.onLoad}
          loaded={props.loaded}
          id={index}
         />
      </li>
    );
  })

  return(
    <>
      <ul className="app__posts list-reset">
        { posts }
      </ul>
      <ReadMore
        postsToShow={props.postsToShow}
        onClick={props.onClick}
      />
    </>
  );
};

export default AppPosts;
