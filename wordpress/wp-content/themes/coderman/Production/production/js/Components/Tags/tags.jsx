import React from 'react';

const Tags = props => {
  if (props.links) {
    const items = props.links.map((item, index) => {
      return <li key={index}>{item}</li>
    });

    return (
      <ul className="card__tags list-reset">
        {items}
      </ul>
    );
  }

  return null;
};

export default Tags;
