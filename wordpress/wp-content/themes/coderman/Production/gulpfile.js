// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const del = require("del");
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const replace = require('gulp-replace');
const htmlmin = require('gulp-htmlmin');
const environments = require('gulp-environments');
const browserSync = require('browser-sync').create();
const imagemin = require("gulp-imagemin");
const webpack = require('webpack-stream');
const wbpck = require('webpack');
const named = require('vinyl-named');
const svgstore = require('gulp-svgstore');
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const svgmin = require('gulp-svgmin');

const responsive = require('gulp-responsive')
const twig = require('gulp-twig');
const yaml = require('yaml')
const data = require('gulp-data');
const fs = require('fs');
const yamlinc = require("gulp-yaml-include");
const cache = require('gulp-cached');

// Settings
const { Settings } = require('./gulp.config.js');

// Production / development mode
const dev = environments.development;
const production = environments.production;

const esLintOptions = {
  parserOptions: {
    ecmaVersion: 2015,
    sourceType: 'module'
  },
  rules: {
    'no-alert': 0,
    'no-bitwise': 0,
    'camelcase': 1,
    'curly': 1,
    'eqeqeq': 0,
    'no-eq-null': 0,
    'guard-for-in': 1,
    'no-empty': 1,
    'no-use-before-define': 0,
    'no-obj-calls': 2,
    'no-unused-vars': 0,
    'new-cap': 1,
    'no-shadow': 0,
    'strict': 2,
    'no-invalid-regexp': 2,
    'comma-dangle': 2,
    'no-undef': 1,
    'no-new': 1,
    'no-extra-semi': 1,
    'no-debugger': 2,
    'no-caller': 1,
    'semi': 1,
    'quotes': 0,
    'no-unreachable': 2
  },
}

// Webpack configuration
const webpackOptions = {
  module: {
    rules: Settings.webpack.rules,
  },
  mode: production() ? 'production' : 'development',
};

/**
 * Markup related tasks
 * (twig, minify)
 */
function markupTask () {
  return src(Settings.path.src + '/*.twig')
  .pipe(data(function (file) {
        return yaml.parse(fs.readFileSync(Settings.path.src + '/data/data.yml', 'utf8'))
      }
    )
  )
  .pipe(twig())
  // .pipe(
  //   production(
  //     htmlmin(
  //       { collapseWhitespace: true }
  //     )
  //   )
  // )
  .pipe(browserSync.stream()) // Inject into browser
  .pipe(dest(Settings.path.dest));
}

/**
 * SVG Icons
 */
function iconsTask () {
  return src(Settings.path.src + '/icons/**/*.svg')
  .pipe(svgstore())
  .pipe(dest(Settings.path.dest + '/icons'))
 }

/**
 * JS
 * (webpack)
 */
function jsTask () {
  return src([
    Settings.path.src + '/js/*.js',
  ])
  .pipe(named())
  .pipe(plumber())
  .pipe(webpack(webpackOptions, wbpck))
  .pipe(src(Settings.path.src + '/js/vendor/*.js'))
  .pipe(browserSync.stream()) // Inject into browser
  .pipe(dest(Settings.path.dest + '/js'))
 }

/**
 * Fonts
 */
 function fontsTask() {
   return src(Settings.path.src + '/fonts/**/*')
   .pipe(dest(Settings.path.dest + '/fonts'))
 }

/**
* Merge YAML files
*/
function mergeYamlTask() {
  return src(Settings.path.src + '/inc/data/*.yml')
  .pipe(yamlinc())
  .pipe(dest(Settings.path.src + '/data'));
}

/**
 * Images
 */
function imagesTask() {

  const resizeOptions = yaml.parse(fs.readFileSync(Settings.path.src + '/sizes.yml', 'utf8'));

  return src(Settings.path.src + '/img/**/*')
  .pipe(cache('images'))
  .pipe(
    imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 5 }),
      imagemin.svgo({
        plugins: [
          {
            removeViewBox: false,
            collapseGroups: true
          }
        ]
      })
    ])
  )
  .pipe(dest(Settings.path.dest + '/img'))
  .pipe(
    responsive(resizeOptions)
  )
  .pipe(dest(Settings.path.dest + '/img'));
}

function browserSyncTask () {
  browserSync.init({
    server: {
      baseDir: './' + Settings.path.dest,
      directory: true
    },
    open: production() ? true : false, // open the browser window
    scrollThrottle: 100 // only send scroll events every 100 milliseconds
  });
}

function stylesTask(){
  return src(Settings.path.src + '/scss/styles.scss')
    .pipe(dev(sourcemaps.init())) // initialize sourcemaps first
    .pipe(sass({
      includePaths: [
        './node_modules/'
        ]
    }).on('error', function (err) {
        return notify().write(err);
      })
    ) // compile SCSS to CSS
    .pipe(
      production(postcss(
        [
          autoprefixer(),
          cssnano()
        ]
      )
    )) // PostCSS plugins
    .pipe(
      dev(
        sourcemaps.write('.') // write sourcemaps file in current directory
      )
    )
    .pipe(browserSync.stream()) // Inject into browser
    .pipe(dest(Settings.path.dest + '/css') // put final CSS in dist folder
  );
}

function watchTask(){
  // Styles
  watch (Settings.path.src + '/**/*.scss', parallel(stylesTask));
  // Icons
  watch (Settings.path.src + '/icons/**/*.svg', parallel(iconsTask));
  // Fonts
  watch (Settings.path.src + '/fonts/**/*', parallel(fontsTask));
  // JS
  watch (Settings.path.src + '/js/**/*', parallel(jsTask));
}

/**
 * Clean 'dist' folder
 */
function cleanTask() {
  return del([Settings.path.dest]);
}

/**
 * Default task
 */
exports.default = series(
  parallel(
    stylesTask,
    iconsTask,
    fontsTask,
    jsTask
  ),
  parallel(
    watchTask,
    // browserSyncTask
  )
);

/**
 * Buld for production
 */
exports.build = series(
  // cleanTask,
  stylesTask,
  iconsTask,
  fontsTask,
  jsTask,
  // browserSyncTask
);

exports.images = series(
  cleanTask,
);

// TODO: Remove - only temporary
exports.markup = series(
  series(
    mergeYamlTask,
    markupTask,
  ),
  // parallel(
  //   watchTask,
  //   // browserSyncTask
  // )
);
