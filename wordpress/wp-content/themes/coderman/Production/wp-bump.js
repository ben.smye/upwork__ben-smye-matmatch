const fs = require('fs');
const PATH = {
  style: '../style.css'
};


const bumpVersion = content => {
  const regex = /\d./g;

  const oldVersionBlock = content.match(regex);

  const newVersion = (parseInt(oldVersionBlock[2]) + 1).toString();

  oldVersionBlock[2] = newVersion;

  const newContent = content.replace(/\d.\d.\d*/g, oldVersionBlock.join(''));

  return newContent;
};

fs.readFile(PATH.style, 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }

  fs.writeFile(PATH.style, bumpVersion(data), function (err) {
    if (err) return console.log(err);
    console.log('Updated theme version');
  });
})




