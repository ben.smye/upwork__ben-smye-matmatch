<?php

$context = Timber::context();

$context['term'] = new Timber\Term();

Timber::render( 'templates/archive.twig', $context );
